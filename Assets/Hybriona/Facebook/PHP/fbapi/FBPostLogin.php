<?php
/* Customize the Page Design here */

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


?>
<h1>Now go back to Game..</h1>
<h3>This page will close in 5 seconds..</h3>

<script>
	function loaded()
	{
    	window.setTimeout(CloseMe, 5000);
	}

	function CloseMe() 
	{
   	 	window.close();
	}
	loaded();
</script>