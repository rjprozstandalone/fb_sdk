﻿using UnityEngine;
using System.Collections;

namespace Hybriona.Facebook
{
	public class HybFacebookConstants 
	{


		private static string m_apiURL= "http://www.hybriona.com/services/fbapi/FBProcess.php";
		public static string apiURL
		{
			get
			{
				return m_apiURL;
			}
		}
		public static void SetApiURL(string url)
		{
			m_apiURL = url;
		}

		public const string APIVERISON = "2.5";
	}
}
