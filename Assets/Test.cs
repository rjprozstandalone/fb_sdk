﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Hybriona.Facebook;
using System.Collections.Generic;

public class Friends
{
	public string Name;
	public void LoadImage(string link)
	{
		HybFacebook.Instance.LoadImage(link,delegate(Texture2D texture) {
			img = texture;

		});
	}
	public Texture2D img;
}
public class Test : MonoBehaviour {


	public Text text;


	public void Login()
	{
		if(!HybFacebook.Instance.IsLoggedIn())
		{
			HybFacebook.Instance.Login ();
			text.text = "Logging Process..";
		}
	}

	public Renderer profilePicRenderer;

	void Start () {

	
		HybFacebookConstants.SetApiURL("http://www.hybriona.com/services/fbapi/FBProcess.php");
		HybFacebook.Instance.Init ("1428281700717324","8d817b4622f7a8bdb9619b508bc9c016","publish_actions,email,user_about_me,public_profile,user_friends");

		HybFacebook.Instance.onLoginFailedOrCancelled += delegate(string reason) {
			
			text.text = "Login Failed!\n"+reason;
		};

		HybFacebook.Instance.onValidatingUser += delegate() {
			text.text = "On Validating User..";
		};



		HybFacebook.Instance.onLoggedInSuccessfully += delegate() {

			text.text = "Login Successful!";
			GetSelfName();
			GetPicture();
			//StartCoroutine(ShareURL());
			StartCoroutine(LoadUploadedPhotos());

		};
	}



	string log = "g";

	Vector2 scroll;
	Friends [] friends = null;
	void OnGUI()
	{
		GUILayout.Label (log);
		scroll = GUILayout.BeginScrollView (scroll);
		if(friends != null)
		{
			for(int i=0;i<friends.Length;i++)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Box(friends[i].img);
				GUILayout.Label(friends[i].Name);
				GUILayout.EndHorizontal();
			}
		}
		GUILayout.EndScrollView ();
	}

	void GetTaggableFriends()
	{
		
		HybFacebook.Instance.API ("me/taggable_friends?limit=1000", HybFacebook.HTTPMethod.GET, delegate(FacebookResponse response) {


			if(response.error == null)
			{
				Dictionary<string,object> res = (Dictionary<string,object> ) Hybriona.MiniJSON.Json.Deserialize( response.text );
				List<object> array = (List<object>) res["data"];
				friends = new Friends[(array.Count>20 ? 20 : array.Count)];
				for(int i=0;i<friends.Length;i++)
				{
					Dictionary<string,object> element = (Dictionary<string,object> )array[i];
					//Debug.Log(element["name"]);
					Dictionary<string,object> picelement =(Dictionary<string,object>)   ((Dictionary<string,object>) element["picture"])["data"] ;

					friends[i] = new Friends();
					friends[i].Name = element["name"].ToString();;
					friends[i].LoadImage(picelement["url"].ToString());

				}
			}
			else
			{
				Debug.Log(response.error);
			}
		}, null);
	}

	void GetSelfName()
	{



		HybFacebook.Instance.API ("me?fields=name,email", HybFacebook.HTTPMethod.GET, delegate(FacebookResponse response) {
			
			if(response.error == null)
			{
				Debug.Log(response.text);
				Dictionary<string,object> res = (Dictionary<string,object> ) Hybriona.MiniJSON.Json.Deserialize( response.text );
				text.text = res["name"].ToString() +"\n"+res["email"].ToString() +"\n";


			}
			else
			{
				Debug.Log(response.error);
				text.text = response.error;
			}
		}, null);

		//GetTaggableFriends();


	}

	public Texture2D tex;
	IEnumerator UploadPhoto()
	{
		yield return new WaitForSeconds(10);
		yield return new WaitForEndOfFrame();
		tex = new Texture2D(Screen.width,Screen.height,TextureFormat.RGB24,false);
		tex.ReadPixels(new Rect(0,0,Screen.width,Screen.height),0,0);
		tex.Apply();

		WWWForm form = new WWWForm();
		form.AddBinaryData("photo",tex.EncodeToJPG(50),"screenshot.jpg");
		form.AddField("message","FB SDK Unity Standalone at "+System.DateTime.Now);
		HybFacebook.Instance.API ("me/photos",HybFacebook.HTTPMethod.POST,delegate(FacebookResponse response) {
			Debug.Log(response.text);
		},form);

		//Destroy(tex);

	}
	IEnumerator LoadUploadedPhotos()
	{
		yield return new WaitForSeconds(5);
		HybFacebook.Instance.API("me/photos/uploaded?fields=name,picture",HybFacebook.HTTPMethod.GET,delegate(FacebookResponse response) {
			Debug.Log(response.text);
		});

	}

	IEnumerator ShareURL()
	{
		yield return new WaitForSeconds(5);

		HybFacebook.Instance.ShareDialog("http://hybriona.com","Hello from hybriona",delegate(string response) {
			Debug.Log(response);
		});

	}
	void GetPicture()
	{

		HybFacebook.Instance.API ("me/picture?type=large", HybFacebook.HTTPMethod.GET, delegate(FacebookResponse response) {
			
			if(response.error == null)
			{

				Texture2D img = new Texture2D(1,1,TextureFormat.ARGB32,false);
				img.LoadImage(response.bytes);
				profilePicRenderer.material.mainTexture = img;
				Vector3 localScale = profilePicRenderer.transform.localScale;
				localScale.x = localScale.y * (float)img.width / (float) img.height;
				profilePicRenderer.transform.localScale = localScale;

			}
			else
			{
				Debug.Log(response.error);
				text.text = response.error;
			}
		}, null);
	}


}
